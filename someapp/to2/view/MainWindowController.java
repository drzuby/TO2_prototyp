package someapp.to2.view;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import someapp.to2.model.Caesar;
import someapp.to2.model.User;
import someapp.to2.presenter.Presenter;


public class MainWindowController {

    private Presenter presenter;
    private Stage stage;

    public void setPresenter(Presenter presenter) {
        this.presenter = presenter;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private TextField usernameTextField;

    @FXML
    private TextField passwordTextField;

    @FXML
    private Button loginButton;

    @FXML
    private Button registerButton;

    @FXML
    private void initialize() {

    }

    @FXML
    public void handleLoginAction(ActionEvent event) {
        User user = new User();
        user.setUsername(usernameTextField.getText());
        user.setPassword(passwordTextField.getText());
        System.out.println(user.getUsername() + "\t" + user.getPassword());

        Caesar caesar = new Caesar();
        presenter.getDatabase().saveToFile("myDatabase.txt", user.getUsername() + "\t" + caesar.encrypt(user.getPassword()) + "\n");

        presenter.showCheckBoxesWindow();

    }

    @FXML
    public void handleRegisterAction(ActionEvent event) {
        presenter.showPopup();
    }

}
