package someapp.to2.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class PopupController {

    private Stage dialogStage;
    private Stage mainStage;

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }

    @FXML
    private Button windowExitButton;

    @FXML
    private Button appExitButton;

    @FXML
    public void handleWindowExitAction(ActionEvent event) {
        dialogStage.close();
    }

    @FXML
    public void handleAppExitAction(ActionEvent event) {

        dialogStage.close();
        mainStage.close();
    }

}
