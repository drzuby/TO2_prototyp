package someapp.to2.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class CheckBoxesController {

    private Stage dialogStage;
    private Stage mainStage;

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }

    @FXML
    CheckBox checkBox1;

    @FXML
    CheckBox checkBox2;

    @FXML
    CheckBox checkBox3;

    @FXML
    Label total;

    @FXML
    Label list;

    @FXML
    public void handleButtonAction(ActionEvent event) {
        int count = 0;
        String choices = "";
        if (checkBox1.isSelected()) {
            count++;
            choices += checkBox1.getText() + "\n";
        }
        if (checkBox2.isSelected()) {
            count++;
            choices += checkBox2.getText() + "\n";
        }
        if (checkBox3.isSelected()) {
            count++;
            choices += checkBox3.getText() + "\n";
        }
        total.setText("Choices amount: " + count);
        list.setText(choices);
    }

}
