package someapp.to2.model;

import java.util.HashSet;
import java.util.Set;
import java.io.*;

public class Database {

    private Set<User> users = new HashSet<>();
    PrintWriter file;

    public Database() {

    }

    public void addUser(User user) {
        this.users.add(user);
    }

    public void saveToFile(String filename, String data) {
        try {
            this.file = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)));
            this.file.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            this.file.close();
        }
    }

}
