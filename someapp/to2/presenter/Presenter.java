package someapp.to2.presenter;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import someapp.to2.Main;
import someapp.to2.model.Database;
import someapp.to2.view.CheckBoxesController;
import someapp.to2.view.MainWindowController;
import someapp.to2.view.PopupController;

import java.io.IOException;

public class Presenter {

    private Stage primaryStage;
    Database database;

    public Presenter(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.database = new Database();
    }

    public Database getDatabase() {
        return this.database;
    }

    public void initRootLayout() {
        try {
            this.primaryStage.setTitle("TO2 prototype - main window");

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/MainWindow.fxml"));
            BorderPane rootLayout = loader.load();

            MainWindowController controller = loader.getController();
            controller.setPresenter(this);

            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showPopup() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/Popup.fxml"));
            BorderPane page = loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Some popup");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            PopupController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainStage(primaryStage);

            dialogStage.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showCheckBoxesWindow() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/CheckBoxes.fxml"));
            AnchorPane page = loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Check Boxes");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            CheckBoxesController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainStage(primaryStage);

            dialogStage.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
